use nom::{error::ParseError, AsChar, IResult, InputTakeAtPosition};

/// Opening tag
pub struct NodeTag {}

///
pub struct PartialNodeTag {}

#[inline]
pub fn is_namestart_char(c: char) -> bool {
    match c {
        ':'
        | 'a'..='z'
        | 'A'..='Z'
        | '\u{C0}'..='\u{F6}'
        | '\u{F8}'..='\u{2FF}'
        | '\u{370}'..='\u{37D}'
        | '\u{37F}'..='\u{1FFF}'
        | '\u{200C}'..='\u{200D}'
        | '\u{2070}'..='\u{218F}'
        | '\u{2C00}'..='\u{2FEF}'
        | '\u{3001}'..='\u{D7FF}'
        | '\u{F900}'..='\u{FDCF}'
        | '\u{FDF0}'..='\u{FFFD}'
        | '\u{10000}'..='\u{EFFFF}' => true,
        _ => false,
    }
}

#[inline]
pub fn is_name_char(c: char) -> bool {
    match c {
        ':'
        | '-'
        | '.'
        | '0'..='9'
        | '\u{B7}'
        | 'a'..='z'
        | 'A'..='Z'
        | '\u{C0}'..='\u{F6}'
        | '\u{F8}'..='\u{2FF}'
        | '\u{370}'..='\u{37D}'
        | '\u{37F}'..='\u{1FFF}'
        | '\u{0300}'..='\u{036F}'
        | '\u{200C}'..='\u{200D}'
        | '\u{203F}'..='\u{2040}'
        | '\u{2070}'..='\u{218F}'
        | '\u{2C00}'..='\u{2FEF}'
        | '\u{3001}'..='\u{D7FF}'
        | '\u{F900}'..='\u{FDCF}'
        | '\u{FDF0}'..='\u{FFFD}'
        | '\u{10000}'..='\u{EFFFF}' => true,
        _ => false,
    }
}

pub fn parse_xml_name(input: &[u8]) -> IResult<&[u8], &str> {
    let target_size = 24;
    let mut split_idx = 0;

    let mut get_next_chunk = || {
        if input.len() <= split_idx {
            return Err(nom::Err::Incomplete(nom::Needed::Unknown));
        }
        let l = (split_idx + target_size).min(input.len());
        let chunk = &input[split_idx..l];
        match std::str::from_utf8(chunk) {
            Ok(s) => {
                split_idx = split_idx + target_size;
                Ok(s)
            }
            Err(e) => match e.valid_up_to() {
                0 => {
                    return Err(nom::Err::Incomplete(nom::Needed::Unknown));
                }
                n => {
                    split_idx = split_idx + n;
                    unsafe { Ok(std::str::from_utf8_unchecked(&chunk[..n])) }
                }
            },
        }
    };

    let mut src = get_next_chunk()?;
    let mut chars = src.chars();

    let char = chars.next().unwrap();
    if !is_namestart_char(char) {
        return Err(nom::Err::Error(nom::error::Error::new(
            input,
            nom::error::ErrorKind::Tag,
        )));
    }

    let mut i = char.len_utf8();

    loop {
        for char in chars {
            if !is_name_char(char) {
                // let name = unsafe { std::str::from_utf8_unchecked(&input[..i]) };
                let name = std::str::from_utf8(&input[..i]).unwrap();
                return Ok((&input[i..], name));
            }

            i += char.len_utf8();
        }

        src = get_next_chunk()?;
        chars = src.chars();
    }
}

#[cfg(test)]
mod parse_xml_name {
    use super::*;

    #[test]
    fn case_foo() {
        let result = parse_xml_name(b"foo bar".as_slice()).unwrap();
        assert_eq!(result, (&b" bar"[..], "foo"));
    }

    #[test]
    fn case_longname() {
        let input =
            b"foosk:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdjflsdjf:sdf euaslkdfja;sdkfjsa;dkfja;sdfbar";
        let result = parse_xml_name(input.as_slice()).unwrap();
        assert_eq!(
            result,
            (
                &b" euaslkdfja;sdkfjsa;dkfja;sdfbar"[..],
                "foosk:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxdjflsdjf:sdf"
            )
        );
    }

    #[test]
    fn case_empty() {
        let result = parse_xml_name(b"".as_slice());
        assert!(result.is_err());
        // assert_eq!(result, (&b" bar"[..], "foo"));
    }

    #[test]
    fn case_len24() {
        let result = parse_xml_name(b"aaaaaaaaaaaaaaaaaaaaaaaa".as_slice());
        assert!(result.is_err());
        // assert_eq!(result, (&b" bar"[..], "foo"));
    }

    #[test]
    fn case_len25() {
        let result = parse_xml_name(b"aaaaaaaaaaaaaaaaaaaaaaaaa".as_slice());
        assert!(result.is_err());
        // assert_eq!(result, (&b" bar"[..], "foo"));
    }
}

// pub node_name(

// pub fn parse_xml_name(input: &[u8]) -> IResult<&[u8], &str>

// pub fn parse_xml_attr_value(input: &[u8]) -> IResult<&[u8], &str> {
//     use nom::branch::alt;
//     use nom::character::streaming::{char, digit0, space0, space1};
//     use nom::sequence::delimited;

//     // match input.position(|item| {
//     //     let c = item.as_char();
//     //     c == '\r' || c == '\n'

//     // alt(
//     //     delimited(char('"'),char('"'))
//     //     // char('\''),
//     // )(input)
// }

// pub fn parse_xml_attr<'input>(input: &'input [u8]) -> IResult<&[u8], &'input str>
// // where
// //     I: InputTakeAtPosition,
// //     <I as InputTakeAtPosition>::Item: AsChar,
// {
//     use nom::character::streaming::{char, digit0, space0, space1};
//     // nom::name(),
//     // char('=')(input)

//     // digit0(input)
//     let (remaining, key) = parse_xml_name(input)?;
//     // let (remaining, _) = char('=')(remaining)?;
//     // let (remaining, value) = parse_xml_attr_value((remaining)?;
//     // Ok((remaining, key))
// }

// [13]   	ubidChar	   ::=   	#x20 | #xD | #xA | [a-zA-Z0-9] | []
/// '"' PubidChar* '"' | "'" (PubidChar - "'")* "'"
// fn parse_pubid_literal(input: &[u8]) -> IResult<&[u8], &str>
// {
//     // char('"') input[0]

//     // let s = std::str::from_utf8(input).unwrap().chars();
//     // match s.next().unwrap() {
//     //     ' ' | '\r' | '\n'
//     //     | '0'..='9'
//     //     | 'A'..='Z'
//     //     | 'a'..='z'| =>

//     // '"' PubidChar* '"' |
// }

///  PubidChar	   ::=   #x20 | #xD | #xA | [a-zA-Z0-9] | [-'()+,./:=?;!*#@$_%]
#[inline]
// fn is_pubid_char(input: &[u8]) -> bool {
fn is_pubid_char<I: AsChar>(input: I) -> bool {
    // let s = std::str::from_utf8().unwrap().chars();
    match input.as_char() {
        ' ' | '\r' | '\n'
        // -'()+,./:=?;!*#@$_%
        | '!' | '#' | '$' | '%'
        // | '\'' | '(' | ')' | '*' | '+' | ','
        // | '-' | '.' | '/' | '0'..='9' | ':' | ';'
        | '\''..=';'
        | '='
          // | '?' | '@' | 'A'..='Z'
        | '?'..='Z'
        | '_'
        | 'a'..='z'
         => true,
        _ => false,
    }
}
// [9]   	EntityValue	   ::=   	'"' ([^%&"] | PEReference | Reference)* '"'
// 			|  "'" ([^%&'] | PEReference | Reference)* "'"
// [10]   	AttValue	   ::=   	'"' ([^<&"] | Reference)* '"'
// 			|  "'" ([^<&'] | Reference)* "'"
// [11]   	SystemLiteral	   ::=   	('"' [^"]* '"') | ("'" [^']* "'")
// [12]   	PubidLiteral	   ::=   	'"' PubidChar* '"' | "'" (PubidChar - "'")* "'"


#[inline]
fn parse_entity_ref(input: &[u8]) -> IResult<&[u8], &str> {
    use nom::character::streaming::{char, digit0, space0, space1};
    use nom::sequence::delimited;
    use nom::sequence::tuple;

    delimited(char('&'), parse_xml_name, char(';'))(input)
}

#[inline]
fn parse_pereference(input: &[u8]) -> IResult<&[u8], &str> {
    use nom::character::streaming::{char, digit0, space0, space1};
    use nom::sequence::delimited;
    use nom::sequence::tuple;

    delimited(char('%'), parse_xml_name, char(';'))(input)
}
//    // / CharRef	   ::=   	'&#' [0-9]+ ';'
// // / 		| '&#x' [0-9a-fA-F]+ ';'

// #[inline]
// fn parse_char_ref(input: &[u8]) -> IResult<&[u8], &str> {
//     use nom::character::streaming::{ char, digit0, hex_digit1, space1};
//     use nom::bytes::streaming::{tag};
//     use nom::sequence::delimited;
//     use nom::sequence::tuple;
//     use nom::branch::alt;

//     alt([
//         delimited(tag("&#x"), hex_digit1, char(';')),
//         delimited(tag("&#"), digit1, char(';'))
//     ])(input)
// }
