
/// <iq> xml stanza
///
/// Used as generic request-response between client/server-server
///
pub struct InfoQuery {
    id: Jid,
    kind: InfoQueryKind,

}


pub struct Jid(String);


pub enum InfoQueryKind {
    Get,
    Set,
    Result,
    Error
}

pub struct StanzaKind {
    Iq(InfoQuery)
}
