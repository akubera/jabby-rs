

use slotmap;
use nom;
use tokio;
use bumpalo;
use bumpalo::{Bump, collections::{Vec as BumpVec, String as BumpString}};
use smallvec;
use tokio_stream::wrappers::IntervalStream;


use std::io::Write;



enum StreamError {
   ConnectionTimeout,
   InvalidFrom,
   InvalidNamespace,
   /// The server is being shut down and all active streams are
   /// being closed
   SystemShutdown,
}

impl StreamError {
   pub fn write_into<W: Write>(&self, w: &mut W) {
      w.write(b"<stream:error>");
      match self {
         ConnectionTimeout => {
            w.write(b"<connection-timeout xmlns='urn:ietf:params:xml:ns:xmpp-streams'/>");
         }
         InvalidFrom => {
            w.write(b"<invalid-from/>");
         }
         InvalidNamespace => {
            w.write(b"<invalid-namespace/>");
         }
         SystemShutdown => {
            w.write(b"<system-shutdown xmlns='urn:ietf:params:xml:ns:xmpp-streams'/>");
         }
      }
      w.write(b"</stream:error>");
   }
}

pub struct XmlTag<'a> {
//    buffer: BumpString<'a>,
//    tag_len: std::num::NonZeroU16,
//    attr_indexes: Vec<(std::num::NonZeroU16, u16)>,
   tag: BumpString<'a>,
   attrs: BumpVec<'a, (BumpString<'a>, BumpString<'a>)>,
}

impl<'bump> XmlTag<'bump> {
   pub fn get_tag(&self) -> &str {
       &self.tag
    //   &self.buffer[..self.tag_len.get() as usize]
   }

   /*
   pub fn iter_attributes(&self) -> impl std::iter::Iterator<Item=(&str, &str)> {
    self.attr_indexes.iter().scan((self.tag_len.get() as usize,0,0),
         |(offset, _kl, _vl), (key_len, val_len)| {
            let kl: usize = key_len.get() as usize;
            let vl: usize = *val_len as usize;
            Some((*offset + kl + vl, kl, vl))
         }
      ).map(
         |(offset, key_len, val_len)| {
            let key_end = offset + key_len;
            let value_end = key_end + val_len;
            (&self.buffer[offset..key_end], &self.buffer[key_end..value_end])
         }
      )
   }
   */

   /// Parse from bytes
//    fn parse<'a, 'b>(buf: &'a [u8], mem: &'b mut bumpalo::Bump) -> (&'a [u8], &'b Self)
   // fn parse<'a>(buf: &'a [u8], mem: &'bump mut bumpalo::Bump) -> (&'a [u8], Self)
   pub fn parse<'a>(buf: &'a [u8], mem: &'bump mut bumpalo::Bump) -> nom::IResult<&'a [u8], Self>
   {
      use nom::bytes::complete::{tag, take_until1, take_till1, take_till};
      use nom::character::complete::{space1, space0, one_of, char, digit1};
      use nom::character::{is_space};

      // dbg!(&buf);

      let (buf, _): (&[u8], char) = char('<')(buf)?;
      let (buf, tag_bytes) = take_till1(is_space)(buf)?;

      let mut attr_buff = smallvec::SmallVec::<[(BumpString, BumpString); 8]>::new();

      loop {
         let (buf, _) = space1(buf)?;
         let (buf, key_bytes) = take_until1("=")(buf)?;
         let (buf, val_bytes) = take_till1(is_space)(buf)?;

         let key = BumpString::from_str_in(std::str::from_utf8(&key_bytes).unwrap(), mem);
         let val = BumpString::from_str_in(std::str::from_utf8(&val_bytes).unwrap(), mem);

         attr_buff.push((key, val));
         break;
      }


      // dbg!(&tag_bytes);

      let tag = BumpString::from_str_in(std::str::from_utf8(&tag_bytes).unwrap(), mem);
      dbg!(&tag);
      let mut attrs = BumpVec::new_in(mem);
      attrs.extend(attr_buff.into_iter());

      Ok((buf,
           Self {
              tag, attrs
               // buffer: String::new(),
               // tag_len: std::num::NonZeroU16::new(8).unwrap(),
               // attr_indexes: vec![],
       }))
   }
}

impl<'b> std::fmt::Debug for XmlTag<'b> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error>
    {
        f.write_str("<XmlTag ")?;
      //   f.write_str(self.get_tag())?;
        f.write_str(&self.tag)?;
      //   for (key, val) in self.iter_attributes() {
        for (key, val) in self.attrs.iter() {
            f.write_str(" ")?;
            f.write_str(&key)?;
            f.write_str("=")?;
            f.write_str(&val)?;
        }
        // self.iter_attributes().map(|(key, val)| {
        //     f.write_str(key)?;
        //     f.write_str(key)?;

        f.write_str(">")
    }
}

pub struct XmlStreamClient<'a> {
//    buffer: BumpString<'a>,
//    tag_len: std::num::NonZeroU16,
//    attr_indexes: Vec<(std::num::NonZeroU16, u16)>,
   tag: BumpString<'a>,
   attrs: BumpVec<'a, (BumpString<'a>, BumpString<'a>)>,
}
