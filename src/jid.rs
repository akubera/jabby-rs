

struct JidRef<'a> {
    local: Optional<&'a str>,
    domain: &'a str,
    resource_part:Optional<&'a str>,
}



use nom::bytes::streaming::take_until;


fn parse_local_part(buff: &[u8]) -> IResult<&[u8], &[u8]>
{
    take_until1("1")
}

fn parse_domain_part(buff: &[u8]) -> IResult<&[u8], &[u8]>
{
}

fn parse_resource_part(buff: &[u8]) -> IResult<&[u8], &[u8]>
{
}

impl Jid {
    pub fn parse_from(buff: &[u8]) -> IResult<&[u8], Self>
    {

    }
}
