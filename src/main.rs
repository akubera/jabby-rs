#![allow(warnings)]


use tokio;
use tokio::net::{TcpListener, TcpStream};
use std::error::Error;
use std::io;
use std::io::Write;
use std::io::BufReader;
use std::marker::Unpin;
use tokio_stream::{self as stream, StreamExt};
use smartstring::{self, SmartString, SmartStringMode};
use nom;
use regex;

use jid;

// extern crate log;

use console_subscriber;
use slotmap;

mod jabber;
mod jab_stream;
mod xml_parsing;
mod xml_structs;


use tracing::info;
use tracing_subscriber;

macro_rules! upon {
   // (($var:ident in $stream:expr) ) => {
   //     while let Some($var) = $stream.next().awai
   // }
   ($var:ident in $stream:expr; {$($block:tt)*}) => {
      while let Some($var) = $stream.next().await {
         $($block)*
      }
   }
}


#[tokio::main]
async fn main() -> io::Result<()>
{
   // let start = std::time::Instant::now();
   // env_logger::Builder::from_default_env().format(move |buf, rec| {
   //    let t = start.elapsed().as_secs_f32();
   //    writeln!(buf, "{:.03} [{}] - {}", t, rec.level(),rec.args())
   // }).init();
   use tracing::{Instrument, Level, span, info_span};

   tracing_subscriber::fmt::init();
   // console_subscriber::init();
   smartstring::validate();
    // let mut stream = stream::iter(vec![0, 1, 2]);
    // upon!( value in stream; {
    //     println!("Got {}", value);
    // });

   info!("Start");

   let listener = TcpListener::bind("127.0.0.1:8080").await?;
   info!("Listening on {:?}", listener.local_addr().unwrap());
   loop {
      let (socket, addr) = listener.accept().await?;
      info!("Opened connection {:?}", addr);
      // let span_name = format!("{}", addr);
      // let span = span!(Level::INFO, span_name);
      let span = tracing::info_span!("", addr=?addr);

      tokio::spawn(handle_connection(socket).instrument(span));
   }
}

use nom::IResult;

async fn handle_connection(socket: TcpStream) -> io::Result<()> {
   match process_socket(socket).await {
      Err(ref e) => {
         info!("Closed connection with error {:?}", e);
      }
      Ok(_) => {
         println!("Closed connection");
      }
   }

   Ok(())
}



struct XmlTag {
   buffer: String,
   tag_len: std::num::NonZeroU16,
   attr_indexes: Vec<(std::num::NonZeroU16, u16)>,
}

impl XmlTag {
   pub fn get_tag(&self) -> &str {
      &self.buffer[..self.tag_len.get() as usize]
   }

   pub fn iter_attributes(&self) -> Vec<(&str, &str)> {
      // https://rust.godbolt.org/z/zcqeoasc5
      /// https://rust.godbolt.org/#z:OYLghAFBqd5TKALEBjA9gEwKYFFMCWALugE4A0BIEAZgQDbYB2AhgLbYgDkAjF%2BTXRMiAZVQtGIHgBYBQogFUAztgAKAD24AGfgCsp5eiyahSAVyVFyKxqiIEh1ZpgDC6embZMQATnLOAGQImbAA5TwAjbFJfcgAHdCViByY3Dy9YhKT7ISCQ8LYomL8bbDsUkSIWUiI0z28S7FscpkrqojywyOjYy3a6jJKqms6Cot8ASmt0M1JUTi44swiAaktzOxWADTZ6ABUWYBWAUgB2ACFjrQBBFbuIsxoaHpXK0mDgcivbu6rgAH1GN41kRMCAQExPODQkIAFrRdAKHgANi%2BNzuKxYRCIpH%2BwRw6mwShAKwAamVjgBmFwQSxgiFQkAwpjw0iIlHkFZmFETKm4NHXM4AEW%2B3wIbDi9G2uwORzOl3RGKWqxoTBWxGi/yxOIIDyIRIgxwATMibDQJisALR8skU6mGk3rTnG0043mU3AnC7fDG%2BtZNGgAOm1uPx2EJSkDGtIEAmgaU4iYUDNgb%2BgOYgeA2CIscxSi5SQAXthyFpS1MfX6MWcaegniorCt/gBreic/4AN3oUxWEGb2AAnummJyOxJh7zTi4vQqflXfYwiCtWySLARiydKULl4Ph5ns7mWPm18WqbP537Fysu6ui9hN9uAFRj%2BjDvMF9fYM%2BVi93EToDgoEfOsaAbE4jXOZcpWNSCu05VtR27XlFXnYUfz9OM2BYOJDRQqsawgECG3g3cgVHccgUnad5XQi8r37IdnAfFYiOzcDIIY4dvzwui2JfMxsH%2BJiqW3TjhIg68KOYbi51/B1TQDQMHieaJjgAVnOViiEDQMxKYTB1KFZ1HUU5TnlIdTzj0zAdP4wTnEM5DZL9NCeJWOMMHoZpY3Q1zBVOIUuCmehuDU/hvC4HRyHQbgACULCXJQZjme9jUpPhyCIbQgqmZsQDUssQq4aR%2BDYfKywiqKYq4fhiTLLLIqC8g4FgFAMBwfBiDIShqDoIF2AWDLBGEMQJE4GQ5GEZQ1E0RryH0I1DGMUwEusJoyhaJx9IGbw1P8fTRm6GIjUWrJkiEHb8viRJzqYQ7Ch6E61uaCphlqdx6iu0pyiENoRmCLoHuOxa%2BhqS69tBjoAbGR6jSmJLZnmbhlRBDYlx2fZDhnH8zJeN4PgFDE0yBEk6XBSEyqZOEESRVEfxDPF9PDIkSXJVAqRpMmGUp5lWXZVEuR5PkBT80UbnFSVpUxuVvTwlHVXVfVcRDXUzH1JR5LNC1rQ9W12ftF0nXA11SHdT0aLcq82DVlj6zYkT/XoINiYzLMcwtI8P1PSlz0vNjraXGYiCWJcHbZ8EAHdiCQf5xDiFhUGIAdaUUhmwwjQMgVjd1Z1olZBFIXtOLIyTX0o9U1RTNOmYz6NY0DLCcII5tyA7Kii/3d33xPEsVmfbu7wmC0LecjF6N3YSt1t0Dsxg4vpJ9vO7ivOyhP05itLn0jpIgl8uMXtzl7YhjmMN0zHnMyytJ06zDJk3ilxfU%2BTKdpSL9UjTrNsiQBLXgy1JFAfUedwg4h0DEsJQSAoAMXIkhe%2B84tLMVXg5IB%2BEAp51INmWYapQFq3gXcUWAUmpFTCuQSq/BqrxUsGsZK8xwKUiNPwBqOgh7kCQNgFgOAYixkMNwEq5AyoFTIdlaK3BaogHqtlVheVKTIkDFoI0WgAAcDDkSnBNDwHwajURFUpOFER1UmFSOaogCAbUAJxAYNEHqEAMASisTEUgPA1JKLLH1JWxIIARBEREYI1QBzcAynYjgwgADyTB6ABLmjgLCJhJDRIIJg8oHYWZzWZqgNWg1%2BDBH1EVKK9BdSkH8W4HAIidRlV4E1GgRhgBKFJAQbAEdQlxGYIE/gw1RDiEkBNDp00NAiP0DwJaJgQDmEsIYXUxJIBTHQHEFoxIuCWlCbVdaP1vAQGcJdIZgRoZHQMGdFoWzrrZBSPdcYQzvotD%2Bu9dI3gLmrKuW9M5PQLlvSOZDZ5MQeDw1oeNTKmCsnBVCvoua1UxlEFQCsHggYXHyN7FQxsnUSCFzSkMlYbh7GMBRUadKEwjGNVYewzhPQeEyOhT4JRPgXGyORLInwWgORFX4WVHgWgKoGLEdYCRmVjEtWQGgCxDibF2MsVi0ZzjXECAYB46g3i5q%2BNYKQKJQSAIhKIOEyJIiYnLXiVFQgST7ApIWVFdJmS2mUGEE0ERBSIhFKVSUhYUVyltKmNUw4dSGlNJaRFIa8hRrdNkL0lQ/S5oLWGStcZNqpk8NmfM7gSyVkvUcBs7aH0MjbIOrsoG%2BybqHLTXc45t1PkGEua9fo%2BaS0PLLf9fIezXnltuSWp5Wbzk/MRpwRaOJsCAt4VwUh5DRFcHBZC6FsKtDwtWisJFZB6GLXRYKrFs68U8oJVMIlXDqC5RAOlQMFKqUqORLS5E9LGV8JBVVTldUV0sKBVwRhwjQWcuYTlVu0QkiOGkEAA
      /*
      let mut offset = self.tag_len.get() as usize;
      let mut output = Vec::with_capacity(self.attr_indexes.len());

      for (key_len, val_len) in self.attr_indexes.iter().map(|(k,v)| (k.get() as usize, *v as usize)) {
         let key_end = offset+key_len;
         let value_end = offset+key_len+val_len;
         let key = &self.buffer[offset..key_end];
         let val = &self.buffer[key_end..value_end];
         output.push((key, val));
         offset = value_end;
      }
      return output;
      */

      /*
      self.attr_indexes.iter().scan(self.tag_len.get() as usize,
         |offset, (key_len, val_len)| {
            // let kl: usize = key_len.get() as usize;
            // let vl: usize = *val_len as usize;
            // Some(vl + kl)
            Some(*offset + key_len.get() as usize + *val_len as usize)
         }
      ).zip(self.attr_indexes.iter()).map(
         |(offset, (key_len, val_len))| {
            let key_end = offset+key_len.get() as usize;
            let value_end = key_end+*val_len as usize;
            (&self.buffer[offset..key_end], &self.buffer[key_end..value_end])
         }
      ).collect()
      */

      self.attr_indexes.iter().scan((self.tag_len.get() as usize,0,0),
         |(offset, _kl, _vl), (key_len, val_len)| {
            let kl: usize = key_len.get() as usize;
            let vl: usize = *val_len as usize;
            Some((*offset + kl + vl, kl, vl))
         }
      ).map(
         |(offset, key_len, val_len)| {
            let key_end = offset + key_len;
            let value_end = key_end + val_len;
            (&self.buffer[offset..key_end], &self.buffer[key_end..value_end])
         }
      ).collect()
   }
}


use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};

// type SmallString = SmartString<<Type as SmartStringMode>::BoxedString>;
type SmallString = String;

struct XmppStreamStream {
   // url: SmartString<SmartStringMode::BoxedString>;
   // id: SmartString<SmartStringMode::BoxedString>;
   pub url: SmallString,
   pub id: SmallString,
}


impl XmppStreamStream {
   // async fn write_preamble<W: AsyncWrite>(&self, stream: &mut tokio::io::BufWriter<W>) -> io::Result<()>
   async fn write_preamble<W: AsyncWriteExt + Unpin>(&self, stream: &mut W) -> io::Result<()>
   {
      // let data = format!("{}{}", )
      // stream.writable().await?;
      // stream.try_write(b"<?xml version='1.0'?>")?;
      // stream.try_write(b"<?xml version='1.0'?>")?;
      // write!(stream, );

      let xmlns = "jabber:client";
      let xmlns_stream = "http://etherx.jabber.org/streams";

      let stream_buff = format!(
         "<stream:stream from='{}' id='{}' xmlns='{}' xmlns:stream='{}' version='1.0'>",
         self.url, self.id, xmlns, &xmlns_stream
      );

      stream.write_all(stream_buff.as_bytes()).await?;
      Ok(())
   }

   // async fn read_preamble(stream: &TcpStream)
}


// use tokio::io::AsyncWriteExt;

// async fn write_into<W: AsyncWriteExt + Unpin>(bytes: &[u8], f: &mut W) -> io::Result<()>
// {
//    // wtr.write(stream_buff.as_bytes()).await?;
//    f.write(bytes).await?;
//    Ok(())
// }

// Client

// async fn read_preamble(stream: &mut TcpStream) ->
// #[tracing::instrument]
async fn process_socket(mut stream: TcpStream) -> io::Result<()>
{
   use tokio::io::BufReader;
   use tokio::io::BufWriter;
   use tokio::io::BufStream;
   use tokio::io;
   use xml_structs::XmppTagBuffer;

   use tracing::{Level, span, info_span};

   // let span = span!(Level::INFO, "my span {}");
   // let _enter = span.enter();
   // info_span!("spamspam");
   let (socket, addr) = listener.accept().await?;
   let xmpp_stream = XmppStreamStream {
      url:"jabby.chat".into(),
      id: "39".into(),
   };

   use bytes::buf::BufMut;

   // let xmlns = "jabber:client";
   // let xmlns_stream = "http://etherx.jabber.org/streams";

   // let stream_buff = format!(
   //    "<stream:stream from='{}' id='{}' xmlns='{}' xmlns:stream='{}' version='1.0'>",
   //    xmpp_stream.url, xmpp_stream.id, xmlns, &xmlns_stream
   // );

   // let attr_map = slotmap::dense::DenseSlotMap::new();

   let buffer = bumpalo::Bump::with_capacity(1024);
   let mut tag_buffer = XmppTagBuffer::with_buffer(buffer);


   let mut bump = bumpalo::Bump::with_capacity(1024);
   // let strm = BufStream::with_capacity(4096, 4096, stream);
   let (mut rdr, wtr) = stream.into_split();

   let mut wtr = BufWriter::with_capacity(1024, wtr);

   use std::io::IoSliceMut;

   // let mut rdr = BufReader::with_capacity(1024, rdr);
   // let mut buf = [0; 10];
   // let mut buf2 = [0; 400];
   // let mut io_buf = IoSliceMut::new(&mut buf);
   // let mut io_buf2 = IoSliceMut::new(&mut buf2);

   let mut buf = Vec::with_capacity(400);

   // // let bufs = [io_buf].as_mut_slice();
   // let mut bufs = [io_buf, io_buf2];
   // let n = rdr.read(&mut buf).await?;
   let n = async {
      let n = rdr.read_buf(&mut buf).await;
      info!("READ {:?} bytes", n);
      n
   }.await?;



   // rdr.readable().await?;
   // let n = rdr.try_read_buf(&mut buf).unwrap();

   use std::io::Cursor;

   // {
   // let parser_input = Cursor::new(buf);
   // }


   // let n = rdr.try_read_vectored(bufs.as_mut_slice()).unwrap();
   // rdr.try_read_buf(&mut bufs)


   info!("read {:?} bytes: {:?}", n, std::str::from_utf8(&buf[..n]).unwrap());

   // println!(">> {:?} {:?}", n, std::str::from_utf8(&buf2[..n]).unwrap());
   // let n = rdr.read(&mut buf).await?;
   // println!(">> {:?} {:?}", n, std::str::from_utf8(&buf[..n]).unwrap());


   let (remaining, xml_info) = XmlVersion::parse_from(&buf[..n]).unwrap();
   info!("parsed XML info {:?}", xml_info);

   if remaining.len() == 0 {
      buf.clear();
   } else {
      let range = buf.len()-remaining.len()..buf.len();
      buf.copy_within(range, 0);
   }

   use ringbuf::RingBuffer;

   let n = rdr.read_buf(&mut buf).await?;

   // let mut buf = [0; 400];
   // let mut buf = RingBuffer::new(400);
   // let (pr, co) = buf.split();
   // rdr.readable().await?;
   // let mm = pr.read_from(&mut rdr, Some(200)).unwrap();

   // let remainng = if remaining.len() == 0 {
      // let n = rdr.read(&mut buf).await?;

      // &buf[..n]
   // } else {
      // remaining
   // };

   println!(">> {:?} {:?}", n, std::str::from_utf8(&buf[..n]).unwrap());

   // let (remaining, xml_info) = parse_xml_decl(&buf[..n]).unwrap();
   let (remaining, xml_tag) = jabber::XmlTag::parse(&buf[..n], &mut bump).unwrap();
   println!(">> {:?} tag={:?} => {:?}", xml_info, xml_tag, std::str::from_utf8(&remaining).unwrap());
   // parse_xml_decl(buff)


   // let n = rdr.read(&mut buf).await?;
   // println!(">> {:?} {:?}", n, std::str::from_utf8(&buf[..n]).unwrap());
   // rdr.readable().await?;
   // let n = rdr.try_read(&mut buf);

   // let n = rdr.get_mut().read(&mut buf).await?;



   // let (mut rdr, mut wtr) = strm.into_split();
   // let (mut rdr, mut wtr) = io::split(strm);

   xmpp_stream.write_preamble(&mut wtr);
   // wtr.write(stream_buff.as_bytes()).await?;


   // rdr.read

   /*

   let mut buf = Vec::with_capacity(4096);

   let mut bufffio = Vec::with_capacity(4096);

   // Wait for the socket to be readable


   // regex::match("

   println!("Read {} bytes: {:?}", n, std::str::from_utf8(&buf).unwrap());

   // IResult
   // let buff = b"<?xml ok";

   let (remaining, version) = match parse_xml_decl(&buf) {
      Ok(pair) => pair,
      Err(e) => {
         dbg!(e);
         return Ok(())
      }
   };

   println!("Parsed {:?}", &version);

   preamble.write_preamble(&mut stream).await?;


   // stream.writable().await?;
   // stream.try_write(b"<?xml version='1.0'?>")?;

   // stream.writable().await?;
   // stream.try_write(b"<stream:stream from='example.com' id='someid' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0'>")?;

   dbg!(&remaining);
   if remaining.len() == 0 {
      stream.readable().await?;
      let n = match stream.try_read_buf(&mut buf) {
         Ok(0) | Err(_) => {
            return Ok(());
         }
         Ok(n) => {
            n
         }
      };
   }
   println!("::: {:?}", std::str::from_utf8(&buf).unwrap());
   // println!("::: {:?}", std::str::from_utf8(remaining.1).unwrap());

   // Wait for the socket to be readable
   stream.readable().await?;

   let mut buff = Vec::with_capacity(4096);
   let n = stream.try_read_buf(&mut buff)?;
   println!(">{} {}", n, String::from_utf8(buff).unwrap());
   */

   // let xml = tag(b"<?xml")(&buf[..n]).unwrap();
//    let n = stream.try_read_buf(&mut buf)
//    let n = stream.read_exact(&mut buf
//                             //    [..10]
//                                ).unwrap()
//        //.await;
//        ;
//    println!("{} {:?}", n, String::from_utf8_lossy(&buf));

   // loop {
   //    buf.clear();
   //    match stream.try_read_buf(&mut buf) {
   //       Ok(0) => break,
   //       Ok(n) => {
   //          println!("read {} bytes", n);
   //          println!("  {:?}", String::from_utf8_lossy(&buf));
   //       }
   //       Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
   //          continue;
   //       }
   //       Err(e) => {
   //          //Err(e.into());
   //          println!("{:?}", e);
   //          return Ok(());
   //       }
   //    }
   // }

   // dbg!(stream);
   return Ok(());
}


#[cfg(test)]
mod test_process_socket {
    use super::*;

    #[tokio::test]
    async fn my_test() {
        assert!(true);
    }
}
