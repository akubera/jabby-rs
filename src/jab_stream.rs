use smartstring::SmartString;
use nom::IResult;

#[derive(Debug)]
pub struct VersionPair {
    major: u8,
    minor: u16,
}

#[derive(Debug)]
pub struct XmlVersion {
    version: VersionPair,
}

impl XmlVersion {
    pub fn parse_from(buff: &[u8]) -> IResult<&[u8], Self>
    {
        use nom::bytes::complete::{tag, take_till, take_till1};
        use nom::character::complete::{char, digit1, one_of, space0, space1};
        use nom::combinator;
        use nom::sequence::tuple;

        let (buff, _) = tag(b"<?xml")(buff)?;
        let (buff, _) = space1(buff)?;
        let (buff, _) = tag(b"version")(buff)?;
        let (buff, _) = space0(buff)?;
        let (buff, _) = char('=')(buff)?;
        let (buff, quote) = one_of("\"'")(buff)?;
        let (buff, _) = tag(b"1.")(buff)?;
        let (buff, version_dec) = digit1(buff)?;
        let (buff, _) = char(quote)(buff)?;
        let (buff, _) = space0(buff)?;
        let (buff, _) = tag(b"?>")(buff)?;
        let (buff, _) = space0(buff)?;

        let minor_version_str = String::from_utf8(version_dec.into()).unwrap();

        Ok((
            buff,
            XmlVersion {
                version: VersionPair {
                    major: 1,
                    minor: minor_version_str.parse().unwrap(),
                },
            },
        ))
    }
}
