// use ringbuffer::{AllocRingBuffer, RingBuffer};

use bumpalo::{Bump, collections::{Vec as BumpVec, String as BumpString}};

/// buffer for XmppTags, katamari style
pub struct XmppTagBuffer {
    buffer: Bump
}

impl XmppTagBuffer {
    pub fn with_buffer(buffer: Bump) -> Self {
        XmppTagBuffer {
            buffer
        }
    }

    pub fn release(mut self) -> Bump {
        self.buffer.reset();
        self.buffer
    }
}
