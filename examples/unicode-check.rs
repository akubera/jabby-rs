#![allow(warnings)]

use tokio;
use tokio::net::{TcpListener, TcpStream};
use tokio::io::{AsyncReadExt};
// use tokio_stream::{self as stream, StreamExt};
use std::io;
use std::io::Write;

use bytes::buf::BufMut;


#[tokio::main]
async fn main() -> io::Result<()>
{
    let listener = TcpListener::bind("127.0.0.1:7070").await?;
    let (mut socket, addr) = listener.accept().await?;

    // let mut buf = [0; 10];
    // socket.read(&mut buf).await?;

    let mut buf = String::with_capacity(100);
    socket.read_to_string(&mut buf).await?;

    println!("{:?}", buf);
    Ok(())
}
