
use tokio;
use tokio::sync;
use tokio::sync::mpsc::{UnboundedSender, UnboundedReceiver};
use std::time::Duration;


// type CallbackType = dyn Fn(i32) -> (dyn Future<Output = ()> + 'static) + 'static;
// type CallbackType = fn(i32) -> dyn Future<Output = ()>;


struct Wrapper<F>
where
    F: std::future::Future
{
    pub ffunc: fn(i32) -> F,
}


unsafe impl<F: std::future::Future> Send for Wrapper<F>
{
}


#[tokio::main]
async fn main()
{
    println!("It works");

    let (tx, rx) = sync::mpsc::unbounded_channel();

    tokio::spawn(waitfor(rx));
    tokio::spawn(sendwhen(tx));

    let wrapped = Wrapper {
        ffunc: work
    };

    tokio::spawn(work(50));

    // call_direct(work);
    tokio::spawn(call_wrapped(wrapped));

    tokio::spawn(async  {
        tokio::time::sleep(Duration::new(1, 0)).await;
    }).await;
}


async fn work(x: i32) {
    let w = Duration::new(2, 0);
    tokio::time::sleep(w).await;
}


// async fn call_direct(fut_func: &dyn Fn(i32) -> (dyn Future<Output = ()> + 'static))
async fn call_direct<F>(fut_func: fn(i32) -> F)
where F: std::future::Future
{}

async fn call_wrapped<F>(fut_func: Wrapper<F>)
where
    F: std::future::Future + Sync + Send
{
    // let fut = (fut_func.ffunc)(42);
    // fut.await;
    tokio::spawn((fut_func.ffunc)(42));
}


/*
async fn call_via_struct<F>(fut_func: Wrapper)
{}
*/

async fn waitfor(mut rx: UnboundedReceiver<u32>)
{
    let x = rx.recv().await;
    dbg!(x);
}

async fn sendwhen(tx: UnboundedSender<u32>)
{
    let w = Duration::new(2, 0);
    tokio::time::sleep(w).await;

    tx.send(42).unwrap();
}
